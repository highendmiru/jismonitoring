package org.gtr.web;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

public class oracleConnectionTest {
	
	private static final String DRIVER =
			"oracle.jdbc.driver.OracleDriver";
	private static final String URL =
			"jdbc:oracle:thin:@10.36.60.17:1521:GTRJIS";
	private static final String USER =
			"TRCC";
	private static final String PW =
			"tr2012";
	
	private static final String DRIVER2 =
			"oracle.jdbc.driver.OracleDriver";
	private static final String URL2 =
			"jdbc:oracle:thin:@10.36.60.149:1521:ORCL";
	private static final String USER2 =
			"TRCC";
	private static final String PW2 =
			"tr2012";
	
	@Test
	public void testConnection() throws Exception{
		Class.forName(DRIVER);
		
		try(Connection con = DriverManager.getConnection(URL, USER, PW)){
			System.out.println(con);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@Test
	public void testConnection2() throws Exception{
		Class.forName(DRIVER2);
		
		try(Connection con = DriverManager.getConnection(URL2, USER2, PW2)){
			System.out.println(con);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
