package org.gtr.web;

import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import org.gtr.service.HomeControllerService;
import org.gtr.service.HomeControllerService2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("*")
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Inject
	private HomeControllerService service;
	
	@Inject
	private HomeControllerService2 service2;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws Exception 
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) throws Exception {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		
		SimpleDateFormat sdf0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		String liveOrDead = "";
		
		InetAddress targetIP = InetAddress.getByName("10.36.60.17");
		boolean reachable = targetIP.isReachable(2000);
		
		if(reachable==true) {
			liveOrDead = "Alive";
		} else {
			liveOrDead = "Dead. please check network";
		}
		
		model.addAttribute("serverTime", sdf0.format(date));
		model.addAttribute("reachable", liveOrDead);
		model.addAttribute(service.read());
		model.addAttribute(service2.read2());
		return "home";
	}
}
