package org.gtr.persistence;

import org.gtr.domain.jisMonitorVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;

@Repository
public class HomeControllerDAOImpl implements HomeControllerDAO{
	  @Autowired
	  @Resource(name="sqlSession")
	  private SqlSession session; 

	  private static String namespace = "org.gtr.mapper.JisMMapper";
	  
	  @Override
	  public jisMonitorVO read() throws Exception {
	    return session.selectOne(namespace + ".read");
	  }
}
