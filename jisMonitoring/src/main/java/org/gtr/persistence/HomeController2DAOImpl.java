package org.gtr.persistence;

import org.gtr.domain.jisMonitorVO2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;

@Repository
public class HomeController2DAOImpl implements HomeController2DAO{
	  @Autowired
	  @Resource(name="sqlSession2")
	  private SqlSession session2;

	  private static String namespace = "org.gtr.mapper.JisMMapper";
	  
	  @Override
	  public jisMonitorVO2 read2() throws Exception {
	    return session2.selectOne(namespace + ".read2");
	  }
}
