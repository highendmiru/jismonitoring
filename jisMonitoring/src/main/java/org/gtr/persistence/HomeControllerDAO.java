package org.gtr.persistence;

import org.gtr.domain.jisMonitorVO;

public interface HomeControllerDAO {

	  public jisMonitorVO read() throws Exception;

}
