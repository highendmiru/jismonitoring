package org.gtr.service;

import javax.inject.Inject;

import org.gtr.domain.jisMonitorVO;
import org.gtr.persistence.HomeControllerDAO;
import org.springframework.stereotype.Service;

@Service
public class HomeControllerServiceImpl implements HomeControllerService{
	  @Inject
	  private HomeControllerDAO dao;
	  
	  @Override 
	  public jisMonitorVO read() throws Exception {
		    return dao.read();
		  }

}
