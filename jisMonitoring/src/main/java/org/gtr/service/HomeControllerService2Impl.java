package org.gtr.service;

import javax.inject.Inject;

import org.gtr.domain.jisMonitorVO2;
import org.gtr.persistence.HomeController2DAO;
import org.springframework.stereotype.Service;

@Service
public class HomeControllerService2Impl implements HomeControllerService2{
	  @Inject
	  private HomeController2DAO dao;
	  
	  @Override
	  public jisMonitorVO2 read2() throws Exception {
		    return dao.read2();
		  }
}
