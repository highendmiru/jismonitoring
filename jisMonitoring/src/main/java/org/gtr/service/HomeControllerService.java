package org.gtr.service;

import org.gtr.domain.jisMonitorVO;

public interface HomeControllerService {
	  public jisMonitorVO read() throws Exception;
}
