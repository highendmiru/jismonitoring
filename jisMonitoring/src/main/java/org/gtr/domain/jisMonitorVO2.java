package org.gtr.domain;

public class jisMonitorVO2 {
	
	private String cmt2;
	private String seq2;
	private String bno2;
	
	public String getCmt2() {
		return cmt2;
	}
	public void setCmt2(String cmt2) {
		this.cmt2 = cmt2;
	}	
	public String getSeq2() {
		return seq2;
	}
	public void setSeq2(String seq2) {
		this.seq2 = seq2;
	}	
	public String getBno2() {
		return bno2;
	}
	public void setBno2(String bno2) {
		this.bno2 = bno2;
	}
	@Override
	public String toString() {
		return "jisMonitorVO2 [cmt2=" + cmt2 +
							", seq2=" + seq2 +
							", bno2=" + bno2 +
							"]";
		
	}
}
