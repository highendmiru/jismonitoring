<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	<meta http-equiv="refresh" content="60">
	<style>
	table {
		border-collapse:collapse;
	}
	table, th {
		border:1px solid white;
		background-color:#163763;	
		color:#FFFFFF;
	}
	td {
		border:1px solid white;
		background-color:#3A72BB;
		color:#000000;
		text-align:center;	
	}
	div.title, div.net {
		font-size:80px;
		text-align:center;
	}
	div.seq {
		background-color:#D0D7E7;
		font-size:80px;
		text-align:center;
	}
	div.diff {
		font-size:120px;
		text-align:center;
		background-color:#7A8EBC;
		color:Green;
	}
	div.diff2 {
		font-size:120px;
		text-align:center;
		background-color:#7A8EBC;
		color:Yellow;
	}
	div.diff3 {
		font-size:120px;
		text-align:center;
		background-color:#7A8EBC;
			color:Red;
	}
	div.color {
		margin-left:2px;
		font-size:20px;
		float:left;
		width:33%;
	}
	</style>
	<script language="javascript">
		var diff_var = ${jisMonitorVO.seq - jisMonitorVO2.seq2};
		
		if(diff_var<5){
			document.getElementsById("diffCT").classList.add("diff");
		} else if(diff_var>=5&&diff_var<10) {
			document.getElementsById("diffCT").classList.add("diff2");
		} else{
			document.getElementsById("diffCT").classList.add("diff3");
		}
	</script>
</head>
<body>
<!-- <P>  The time on the server is ${serverTime}. </P>  -->
<table width=100% border="1" cellspacing="0" cellpadding="0" bordercolor="#000000" style="border-collapse:collapse">
	<tr>
		<td colspan="3"><div class="title">GTR JIS CHECK SYSTEM </div></td>
	</tr>
	<tr>
		<th width=40%><div class="title">MAIN</div></th>
		<th rowspan="2" style="background-color:#7A8EBC;"><div class="diffCT"><div class="diff">${jisMonitorVO.seq - jisMonitorVO2.seq2}</div></div></th>
		<th width=40%><div class="title">BACKUP</div></th>
	</tr>
	<tr>
		<td width=40%><div class="seq">${jisMonitorVO.seq}</div></td>
		<td width=40%><div class="seq">${jisMonitorVO2.seq2}</div></td>
	</tr>
	<tr>
		<td colspan="3" align=center>
			<br/>
			<div class="color" style="color:Green;">GREEN : Below 4 Cars Difference </div> 
			<div class="color" style="color:Yellow;">YELLOW : 5~9 Cars Difference </div> 
			<div class="color" style="color:Red;">RED : Over 10 Cars Difference</div>
			<br/>
			<br/>
		</td>
	</tr>
	<tr>
		<td colspan="3"><div class="net">Network is ${reachable}</div></td>
	</tr>
	<tr>
		<td colspan="3"><div class="time" style="font-size:20px;">${serverTime}</div></td>
	</tr>
</table>
</body>
</html>
